package Description;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Give me a number.");
        int a = s.nextInt();
        if (a > 0) {
            System.out.println("The number is positive.");
        }
        else if (a < 0) {
            System.out.println("The number is negative.");
        }
        else {
            System.out.println("It's null");
        }
        if ((a % 2) == 0) {
            System.out.println("It's the even number.");
        }
        else {
            System.out.println("It's not the even number.");
        }
    }
}
