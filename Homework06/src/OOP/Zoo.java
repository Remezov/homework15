package OOP;

import OOP.Animal.Baboon;
import OOP.Animal.Bear;
import OOP.Animal.Macaque;
import OOP.Animal.WhiteBear;

public class Zoo {
    public static void main(String[] args) {
        Baboon baboon = new Baboon("Mimi", "eat a banana");
        System.out.println(baboon.call());
        Macaque macaque = new Macaque("Kuka","comb out fleas");
        System.out.println(macaque.call());
        WhiteBear whiteBear = new WhiteBear("Teddy", "waterpool");
        System.out.println(whiteBear.call());
        System.out.println(whiteBear.dream());

    }
}
