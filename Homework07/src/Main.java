import VM.*;

import java.io.FileInputStream;
import java.util.Scanner;
import java.util.logging.*;

public class Main {
    final static Logger logger = Logger.getLogger(Main.class.getName());
    public static void main(String[] args) throws Exception {
        LogManager.getLogManager().readConfiguration(new FileInputStream("logging.properties"));
        Scanner s = new Scanner(System.in);
        VM vm = new VM();
        int money = 0;
        logger.info("начали работу программы");
        while (true) {
            vm.table1();
            int choice1 = s.nextInt();
            logger.info("выбор позиции");
            switch (choice1) {
                case 1:
                    vm.table2();
                    break;
                case 2:
                    System.out.println("\nEnter the money");
                    money = s.nextInt();
                    logger.info("ввод денег");
                    vm.enteringMoney(money);
                    break;
                case 3:
                    System.out.println("\nChoose a drink");
                    int choice2 = s.nextInt();
                    logger.info("выбор напитка");
                    switch (choice2) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            vm.deliveryOfGoods(choice2, money);
                            money = 0;
                            break;
                        default:
                            System.out.println("Incorrect number. Get your money.");
                            logger.warning("введен некорректный номер напитка");
                            money = 0;
                    }
                    break;
                default:
                    System.out.println("Incorrect number.");
                    logger.warning("введен некорректный номер главного меню");
            }
        }

    }
}
