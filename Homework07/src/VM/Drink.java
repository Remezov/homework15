package VM;

public enum Drink {
    TEA(10), COFFEE(20), CAPPUCCINO(30), KARKADE(15);

    private int cost;

    Drink(int cost) {
        this.cost = cost;
    }

    public int getCost() {
        return cost;
    }
}
