package VM;

import java.util.logging.Logger;

public class VM {
    final static Logger logger = Logger.getLogger(VM.class.getName());
    public VM() {
    }

    public void table1()
    {
        System.out.println("\n1 - Menu");
        System.out.println("2 - Put your money");
        System.out.println("3 - Choose a drink");
        logger.info("выведенно главное меню");
    }
    public void table2()
    {
        for (int i = 0; i <= 3; i++)
        {
            System.out.println(i+1+ " " +Drink.values()[i]+ " " +Drink.values()[i].getCost()+" RUB");
        }
        logger.info("выведенно меню напитков");
    }
    public void deliveryOfGoods(int number, int money){
        if (money >= Drink.values()[number - 1].getCost()) {
            System.out.println("Get your "+Drink.values()[number - 1]+" and change");
            logger.info("выдача напитка");
        }
        else {
            System.out.println("Insufficient funds. Get your money.");
            logger.warning("не хватает средств");
        }
    }
    public void enteringMoney(int money)
    {
        System.out.println("\n Your limit is "+money+" RUB");
    }
}
