package ru.remezov.calculator;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        int number = 0;

        System.out.println("1 - sum\n2 - substraction\n3 - multiplication\n4 - division\n5 - percent\n6 - exit");

        while(number != 6) {

            System.out.println("\nEnter operation number");
            number = s.nextInt();

            switch (number) {
                case 1:
                    System.out.println("Enter two numbers");
                    Calculator.sum(s.nextDouble(), s.nextDouble());
                    break;
                case 2:
                    System.out.println("Enter two numbers");
                    Calculator.substraction(s.nextDouble(), s.nextDouble());
                    break;
                case 3:
                    System.out.println("Enter two numbers");
                    Calculator.multiplication(s.nextDouble(), s.nextDouble());
                    break;
                case 4:
                    System.out.println("Enter two numbers");
                    Calculator.division(s.nextDouble(), s.nextDouble());
                    break;
                case 5:
                    System.out.println("Enter two numbers");
                    Calculator.percent(s.nextDouble(), s.nextDouble());
                    break;
                default:
                    break;
            }

        }
    }
}
