package ru.remezov.baby;


import static ru.remezov.baby.Baby.Food.*;

public class Baby {

    enum Food{CAKE, APPLE, SOAP, PORRIDGE}
    Food food;

    public void eat(Food food) {

        try {
            switch (food) {
                case CAKE:
                case APPLE:
                    System.out.println("The child has eaten for the both cheeks.");
                    break;
                case SOAP:
                case PORRIDGE:
                    throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("The child has spat out!");
        } finally {
            System.out.println("'Thank you mom!'");
        }
    }
}
