package ru.remezov.baby;

import static ru.remezov.baby.Baby.Food.*;

public class Mother {
    public static void main(String[] args) {

        Baby baby = new Baby();
        //CAKE, APPLE, SOAP, PORRIDGE
        baby.eat(SOAP);
    }
}
