package ru.remezov.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class Main {
    public static void main(String[] args) {
        File file = new File("test/file.txt");
        try {
            file.createNewFile();
        } catch (IOException ex) {
            ex.getMessage();
        }
        file.renameTo(new File("test/file1.txt"));
        try {
            Files.copy(file.toPath(), new File("test/fileCopy.txt").toPath());
        } catch (IOException ex) {
            ex.getMessage();
        }
        file.delete();
    }
}
