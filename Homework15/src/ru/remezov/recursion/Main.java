package ru.remezov.recursion;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        try {
            new File("test2/a/b/c/d").mkdirs();
            new File("test2/a/b/c/d/1.txt").createNewFile();
        } catch (Exception ex){
            ex.getMessage();
        }
        bypass(new File("test2"),"");
    }
    private static void bypass(File root, String depth) {
        String result = depth;
        if (root != null && root.exists()) {
            System.out.println(result + root.getName());
            if(root.isDirectory()) {
                for (File file : root.listFiles()) {
                    bypass(file,depth + " ");
                }
            }
        }
    }
}
