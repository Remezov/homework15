package ru.remezov.palindrome1;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Enter a word, a number or a sentence (no punctuation)");
        String word = s.nextLine();
        String word0 = word.replaceAll(" ","");
        String word1 = word0.toLowerCase();
        StringBuilder word2 = new StringBuilder(word1);
        String word3 = word2.reverse().toString();
        if (word1.equals(word3)) {
            System.out.println("It's palindrome.");
        }
        else {
            System.out.println("It's not palindrome.");
        }
    }
}
